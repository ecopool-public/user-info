@echo off

cd C:
cd %USERPROFILE%\AppData\Local\chia-blockchain\app-1.1.7\resources\app.asar.unpacked\daemon

rem Размерность плота
rem Dimension of the plot

set K_SIZE=32

rem Количество потоков
rem Number of threads

set THREADS_NUM=4

rem Количество оперативной памяти на плот
rem Amount of RAM plot

set RAM_SIZE=12288

rem Путь к папке для временных файлов (обычно ssd/nvme/raid0)
rem The path to the folder for temporary files (usually ssd/nvme/raid0)

set FIRST_TEMP_FOLDER=C:\temp1

rem Путь ко второй папке для временных файлов (если не отличается от первой - установите аналогичное значение)
rem Path to the second folder for temporary files (if it does not differ from the first, set the same value)

set SECOND_TEMP_FOLDER=C:\temp1

rem Путь к директории в которой будет храниться конечный плот
rem The path to the folder where you want to put the finished plot

set DESTINATION_FOLDER=C:\dest

rem Количество плотов ПОДРЯД (последовательно), которые надо создать
rem The number of ROW rafts (sequentially) to create
set PLOTS_AMOUNT=2

start chia.exe plots create -k %K_SIZE% -f 97d571995423d6c5620afcf3b8a9e29f95dcb4f5a94ee6df67cc6edebec2e17851937ed62c054783e0f1bd007400851c -p a8f696e15a43cd45c5bf142b75de328a1931d926bd17ee11ca8c9db7d7f34cecacd1010db0c69c2f39cdde126cc05d2c -r %THREADS_NUM% -b %RAM_SIZE% -t %FIRST_TEMP_FOLDER% -2 %SECOND_TEMP_FOLDER% -d %DESTINATION_FOLDER% -n %PLOTS_AMOUNT%

echo All plots created
echo Press any key to continue
pause > nul
