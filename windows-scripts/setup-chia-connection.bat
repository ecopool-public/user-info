@echo off

mkdir C:\ca_pool
curl https://gitlab.com/ecopool-public/user-info/-/raw/master/certs/chia_ca.crt --output C:\ca_pool\chia_ca.crt
curl https://gitlab.com/ecopool-public/user-info/-/raw/master/certs/chia_ca.key --output C:\ca_pool\chia_ca.key
curl https://gitlab.com/ecopool-public/user-info/-/raw/master/certs/private_ca.crt --output C:\ca_pool\private_ca.crt
curl https://gitlab.com/ecopool-public/user-info/-/raw/master/certs/private_ca.key --output C:\ca_pool\private_ca.key

cd C:
cd %USERPROFILE%\AppData\Local\chia-blockchain\app-1.1.7\resources\app.asar.unpacked\daemon

.\chia.exe init
.\chia.exe keys generate 
.\chia.exe init -c C:\ca_pool

@RD /S /Q C:\ca_pool

echo Setup certificates is done
echo Press any key to continue
pause > nul

echo Setting up chia harvester connection to pool

chia configure --enable-upnp false
chia configure --set-farmer-peer 136.243.104.155:8447
chia configure --set-log-level DEBUG

echo Setup connection done
echo Press any key to continue
pause > nul
