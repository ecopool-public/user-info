@echo off

cd C:\

rem git clone https://gitlab.com/ecopool-public/user-info.git


CMD /C curl https://www.python.org/ftp/python/3.9.5/python-3.9.5-amd64.exe -o python-3.9.5-amd64.exe
CMD /C .\python-3.9.5-amd64.exe


CMD /C curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
CMD /C python .\get-pip.py

CMD /C pip install tzlocal

cd C:\machinaris

docker build -t plotter .

pause >nul

